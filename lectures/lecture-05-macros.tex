\documentclass{latexkurs}

\subtitle{Makros und Debugging}
\date{\dateSixthLecture}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Ziele}

  \onslide<+->

  \begin{itemize}
  \item<+-> Einen kleinen Einblick gewinnen, wie \LaTeX\ (\TeX) funktioniert
  \item<+-> Lernen, eigene Makros und Umgebungen zu schreiben
  \item<+-> \LaTeX-Fehlermeldungen verstehen und beheben
  \end{itemize}

\end{frame}

\section{Wie funktionieren \LaTeX-Makros?}

\begin{frame}[fragile]
  \frametitle{Wie \LaTeX\ den Quelltext sieht}

  \onslide<+->

  \TeX\ (und damit auch \LaTeX) arbeiten mit Hilfe von \emph{Makros}

  \onslide<+->
  \begin{Beispiel}
    Wenn \TeX\ das Makro
\begin{lstlisting}
\TeX
\end{lstlisting}
    im Quelltext sieht, wird dieses \emph{expandiert} durch\onslide<+->
\begin{lstlisting}
T\kern -.1667em\lower .5ex\hbox {E}\kern -.125emX.
\end{lstlisting}
  \end{Beispiel}

  \onslide<+->

  \begin{block}{Expansion}
    \begin{itemize}
    \item<+-> Expandiert werden nur \emph{definierte} Makros.
    \item<+-> \emph{Primitive} Makros werden direkt von \TeX\ verarbeitet
    \item<+-> Menge der primitiven Makros ist fest, definierte Makros können vom
      Benutzer hinzugefügt werden
    \end{itemize}
  \end{block}

\end{frame}

\section{Makrodefinitionen}

\begin{frame}[fragile]
  \frametitle{Makrodefinitionen in \LaTeX}

  Zur Definition neuer Makros stehen in \LaTeX\ drei Arten von Makros bereit:
  \onslide<+->
  \begin{itemize}
  \item<+-> \lstinline|\newcommand| zur Definition neuer Makros
  \item<+-> \lstinline|\renewcommand| zur Neudefinition bereits bestehender
    Makros
  \item<+-> \lstinline|\providecommand| zur Definition eines Makros, sofern dies
    noch nicht definiert ist
  \item<+-> \texttt{*}-Varianten, die Makros definieren, deren Argumente keine
    Absätze enthalten dürfen
  \item<+-> \lstinline|\DeclareRobustCommand| zur Definition
    \emph{nicht-zerbrechlicher} Befehle
  \end{itemize}

  \onslide<+->

  \begin{Beispiel}
\begin{lstlisting}
\newcommand{\bsp}{beispielsweise}
\end{lstlisting}
  \end{Beispiel}

  \onslide<+->

  \begin{block}{Hinweis}
    Makrodefinitionen sollten in der Präambel stehen.
  \end{block}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Argumente}

  \onslide<+->

  Makros können auch \emph{Argumente} bekommen

  \onslide<+->

  \begin{Beispiel}
\begin{lstlisting}
\newcommand{\inBlau}[1]{\textcolor{blue}{#1}}
\end{lstlisting}
  \end{Beispiel}

  \onslide<+->

  \begin{block}{Allgemein gilt}
    \begin{itemize}
    \item<+-> Argumente beginnen mit \texttt{\#}
    \item<+-> Erlaubt sind maximal 9 Argumente
    \item<+-> Argumente können beliebig oft und in beliebiger Reihenfolge in der
      Makrodefinition verwendet werden
    \end{itemize}
  \end{block}

  \onslide<+->

  \medskip{}

  \begin{block}{Hinweis}
    \begin{itemize}
    \item<+-> Alle Argumente sind obligatorisch
    \item<+-> \lstinline|\newcommand| kann auch Makros mit einem optionalen
      Argument definieren.
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Andere Arten der Makrodefinition}

  \onslide<+->
  \onslide<+->

  \begin{block}{In \TeX}
    \begin{itemize}
    \item<+-> Mittels \lstinline|\def|:
\begin{lstlisting}
\def\inBlau#1{\textcolor{blue}{#1}}
\end{lstlisting}
    \item<+-> Definitionen flexibler, aber auch fehleranfälliger
    \end{itemize}
  \end{block}

  \onslide<+->

  \begin{block}{In \LaTeX3}
    \begin{itemize}
    \item<+-> Mit dem Kommando \lstinline|\DeclareDocumentCommand|
\begin{lstlisting}
\DeclareDocumentCommand \chapter { s o m } {
  \IfBooleanTF {#1}
    { \typesetstarchapter {#3} }
    { \typesetnormalchapter {#2} {#3} } }
\end{lstlisting}
    \item<+-> Ermöglicht explizite Angabe der \emph{Signatur} eines Makros
    \item<+-> Mit Paket \lstinline|xparse| in \LaTeX\ verwendbar
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Umgebungsdefinitionen}

  \onslide<+->

  Neue Umgebungen können ebenfalls definiert werden \onslide<+->

  \begin{Beispiel}
\begin{lstlisting}
\newenvironment{sketch}[1]
  {\begingroup\color{#1}}       % begin-Definition
  {\endgroup}                   % end-Definition
...
\begin{sketch}{gray}
  ...
\end{sketch}
\end{lstlisting}
  \end{Beispiel}

  \medskip{}

  \onslide<+->

  Argumente können nur im \texttt{begin}-Teil genutzt werden.

\end{frame}


\section{Ausblick: \enquote{Programmieren} in \TeX}

\begin{frame}[fragile]
  \frametitle{Plain\TeX}

  \small

\begin{lstlisting}[frame=none]
\newif\ifprime \newif\ifunknown                 % boolean variables
\newcount\n \newcount\p \newcount\d \newcount\a % integer variables
\def\primes#1{2,~3%                  % assume that #1 is at least 3
  \n=#1 \advance\n by -2             % n more to go
  \p=5                               % odd primes starting with p
  \loop\ifnum\n>0 \printifprime\advance\p by 2\repeat}
\def\printp{,               % we will invoke \printp if \p is prime
  \ifnum\n=1 and~\fi        % `and' precedes the last value
  \number\p \advance\n by -1 }
\def\printifprime{\testprimality \ifprime\printp\fi}
\def\testprimality{{\d=3 \global\primetrue
  \loop\trialdivision \ifunknown\advance\d by 2\repeat}}
\def\trialdivision{\a=\p \divide\a by \d
  \ifnum\a>\d \unknowntrue\else\unknownfalse\fi
  \multiply\a by \d
  \ifnum\a=\p \global\primefalse\unknownfalse\fi}

The first thirty prime numbers are \primes{30}. \bye
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Lua\LaTeX}

  \onslide<+->

  \footnotesize

\begin{lstlisting}[frame=none]
\documentclass[tikz,border=10pt]{standalone}
\usepackage{pgfplots}
\usepackage{luacode}

\begin{luacode}
  function weierstrass(x0, x1, n, a, b, epsilon)
    local out = assert(io.open("tmp.data", "w"))
    ...
\end{luacode}

\begin{document}

\directlua{weierstrass(-2,2,500,0.3,5,1.e-12)}
\begin{tikzpicture}
  \begin{axis}[axis lines=middle, ymin=-1.5, ymax=1.75]
    \addplot[thick] table {tmp.data};
  \end{axis}
\end{tikzpicture}

\end{document}
\end{lstlisting}

\end{frame}


\section{Debugging}

\begin{frame}[fragile]
  \frametitle{Fehlermeldungen in \LaTeX}

  \onslide<+->

  \begin{block}{Problem \dots}
    \begin{itemize}
    \item<+-> Fehlermeldungen in \LaTeX\ sind meist schwer verständlich
    \item<+-> Ursache: Fehler werden meist oft erst erkannt, \emph{nachdem} alle
      Makros expandiert worden sind
    \item<+-> Fehlerbeschreibung ist deswegen meist \emph{nicht hilfreich}
    \end{itemize}
  \end{block}

  \onslide<+->

  \begin{block}{Allgemeine Lösungstrategien}
    \begin{itemize}
    \item<+-> Ordnung im \TeX-Dokument
    \item<+-> Fehlereinkreisung durch \enquote{binäre Suche}
    \item<+-> \lstinline|\RequirePackage{nag}| zum Auffinden veralteter Befehle
    \item<+-> Verwendung von externen \emph{Prüfprogrammen} wie \texttt{lacheck}
      oder \texttt{chktex}
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]
  \frametitle{\enquote{Klassische} Fehlermeldungen}

  \onslide<+->

  \begin{itemize}
  \item<+-> Schließende \} ohne dazu passende, öffnende \{

\begin{verbatim}
! Too many }'s.
l.6 \date December 2004}
\end{verbatim}

  \item<+-> Undefinierter Befehl (meistens vertippt)

\begin{verbatim}
! Undefined control sequence.
l.6 \dtae
{December 2004}
\end{verbatim}

  \item<+-> Mathematikbefehl außerhalb des Mathematikmodus' benutzt

\begin{verbatim}
! Missing $ inserted
\end{verbatim}

  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{\enquote{Klassische} Fehlermeldungen}
  \onslide<+->

  \begin{itemize}
  \item<+-> Unerlaubter Absatz im Argument eines Makros

\begin{verbatim}
Runaway argument?
{December 2004 \maketitle
! Paragraph ended before \date was complete.
<to be read again>
\par
l.8
\end{verbatim}

  \item<+-> Fehlendes \lstinline|\item| in Aufzählung

\begin{verbatim}
! LaTeX Error: Something's wrong--perhaps a missing \item.
...
l.37   \end{itemize}
\end{verbatim}
  \end{itemize}

  \onslide<+->

  Mehr Hilfe unter
  \url{https://en.wikibooks.org/wiki/LaTeX/Errors_and_Warnings}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Mehr Informationen bei Fehlern}

  \onslide<+->

  \TeX\ (und damit auch \LaTeX) kann dazu gebracht werden, bei Fehlern mehr
  Informationen auszugeben.

  \begin{itemize}
  \item<+-> \lstinline|\errorcontextlines=5| im Dokument sorgt dafür, dass bei
    Fehlern die ersten 5 Expansionsstufen angezeigt werden.
  \item<+-> \lstinline|\listfiles| in der Präambel zeigt die Versionen aller
    geladenen Pakete an
  \item<+-> Viele \emph{Tracing-Befehle} sind direkt in \TeX\ eingebaut
\begin{lstlisting}
\tracingmacros=1
\tracingcommands=1
\tracingall
\end{lstlisting}
    (Siehe auch
    \url{https://tex.stackexchange.com/questions/60491/latex-tracing-commands-list})
  \item<+-> \lstinline|\usepackage{trace}|
  \end{itemize}

\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: luatex
%%% End:

%  LocalWords:  Makrodefinition Umgebungsdefinitionen
