# Einführung in LaTeX

Dies sind die LaTeX Quelldateien für den Kurs *Einführung in LaTeX*, der
regelmäßig an der [HTW Dresden](htw-dresden.de) von [Tom Hanika](Tom)
und [Daniel Borchmann](Daniel) gehalten wird.  Alle Inhalte sind, sofern nicht
anders angegeben, unter
einer [Creative Commons Attribution-ShareAlike 3.0 Unported License](CC-BY-SA)
lizensiert.

[Tom]: http://www.kde.cs.uni-kassel.de/hanika
[Daniel]: http://wwwtcs.inf.tu-dresden.de/~borch
[CC-BY-SA]: http://creativecommons.org/licenses/by-sa/3.0/deed.en_US

ⓒ 2015—2016 Daniel Borchmann, Tom Hanika
